package Propertiy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Class PropertyValues
 */

public class PropertyValues {
    private String propFileName = "pictureit.properties";
    private static Properties prop = new Properties();

    /**
     * Method for getting username to database
     *
     * @return username
     * @throws IOException .
     */
    public String getDBusername() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("username");
    }

    /**
     * Method for getting password to database
     *
     * @return password
     * @throws IOException .
     */
    public String getDBpassword() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("password");
    }

    /**
     * Method for getting map key
     *
     * @return map kay
     * @throws IOException .
     */
    public String getMapKey() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("key");
    }

    public String getCloudinaryName() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("name");
    }

    public String getCloudinaryKey() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("cloudKey");
    }

    public String getCloudinarySecret() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        prop.load(inputStream);
        inputStream.close();
        return prop.getProperty("secret");
    }

    public Map getProperties() throws IOException {
        Map propertiesMap = new HashMap<>();
        propertiesMap.put("cloud_name", getCloudinaryName());
        propertiesMap.put("api_key", getCloudinaryKey());
        propertiesMap.put("api_secret", getCloudinarySecret());
        return propertiesMap;
    }
}
