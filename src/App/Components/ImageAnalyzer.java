package Components;

import Database.HibernateClasses.Picture;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Class ImageAnalyzer
 */
public class ImageAnalyzer {

    /**
     * Used to analyze an image
     *
     * @param url the image URL
     * @return a picture
     * @throws IOException          Is thrown if there is something wrong with the url
     * @throws NullPointerException Is thrown if some of the metadata that cant be null, is null
     */
    public static Picture analyze(String url) throws IOException, NullPointerException {
        URL path = new URL(url);
        URLConnection con = path.openConnection();
        InputStream in = con.getInputStream();
        javaxt.io.Image image = new javaxt.io.Image(in);
        java.util.HashMap<Integer, Object> exif = image.getExifTags();

        double[] coord = image.getGPSCoordinate();
        Picture picture = new Picture();
        picture.setUrl(url);
        picture.setDate((String) exif.get(0x0132));
        if (coord != null) {
            picture.setLatitude(coord[1]);
            picture.setLongitude(coord[0]);
        }
        picture.setHeight(image.getHeight());
        picture.setWidth(image.getWidth());
        picture.setFileSize(con.getContentLength());
        picture.setFileType(con.getContentType());
        return picture;
    }
}