package Components;

import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Class PDF
 */
public class PDF {

    /**
     * Creates a PDF document based on an album, and adds table with content
     *
     * @param pictures a list of the picture ids from the album
     * @param name     name of the album
     * @throws IOException       Is thrown if there is something wrong with the url
     * @throws DocumentException Is thrown if an error has occurred in a Document
     */
    public void createPDF(ArrayList<String> pictures, String name) throws IOException, DocumentException {
        Document document = new Document();
        Stage popUp = new Stage();
        popUp.initModality(Modality.APPLICATION_MODAL);
        FileChooser fil_chooser = new FileChooser();
        fil_chooser.setInitialFileName(name);
        fil_chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF", "*.pdf"));
        File file = fil_chooser.showSaveDialog(popUp);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(file));
        } catch (NullPointerException ignored) {
        }
        document.open();
        document.add(createTable(name, pictures));
        document.close();
        Desktop.getDesktop().open(file);
    }

    /**
     * Creates a table with title and images
     *
     * @param name     Name of the album
     * @param pictures Picture ids from the album
     * @return table, Returns the table with content
     * @throws IOException         Is thrown if there is something wrong with the url
     * @throws BadElementException Is thrown if the Element do not have the right form
     */
    private PdfPTable createTable(String name, ArrayList<String> pictures) throws IOException, BadElementException {
        PdfPTable table = new PdfPTable(1);
        addTitle(table, name);
        addImages(table, pictures);
        return table;
    }

    /**
     * Adds the album title to a table
     *
     * @param table A table to add the title to
     * @param name  Name of the album
     */
    private void addTitle(PdfPTable table, String name) {
        Font f = new Font(Font.FontFamily.TIMES_ROMAN, 50.0f);
        Paragraph p = new Paragraph(name, f);
        PdfPCell cell1 = new PdfPCell(p);
        cell1.setPaddingBottom(35);
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell1.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell1);
    }

    /**
     * Adds images from the album to a table
     *
     * @param table    A table to add the pictures to
     * @param pictures Pictures from the album
     * @throws IOException         Is thrown if there is something wrong with the url
     * @throws BadElementException Is thrown if the Element do not have the right form
     */
    private void addImages(PdfPTable table, ArrayList<String> pictures) throws IOException, BadElementException {
        for (String imageUrl : pictures) {
            Image image = Image.getInstance(new URL(imageUrl));
            image.scaleAbsolute(150f, 150f);
            table.addCell(new PdfPCell(image, true));

            //Adds a blank row to create space between each image
            PdfPCell blankRow = new PdfPCell(new Phrase("\n"));
            blankRow.setFixedHeight(30f);
            blankRow.setColspan(4);
            blankRow.setBorder(Rectangle.NO_BORDER);
            table.addCell(blankRow);
        }
    }

}

