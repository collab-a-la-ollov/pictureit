package Database;

import Propertiy.PropertyValues;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * The Hibernate class
 */
public class Hibernate {
    private static EntityManagerFactory ENTITY_MANAGER_FACTORY;

    static {
        try {
            ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("Database", getProperties());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();


    /**
     * Gets Entity manager
     *
     * @return the em
     */
    public static EntityManager getEm() {
        return em;
    }

    /**
     * Gets entity manager factory.
     *
     * @return the entity manager factory
     */
    public static EntityManagerFactory getEntityManagerFactory() {
        return ENTITY_MANAGER_FACTORY;
    }

    /**
     * @return
     * @throws IOException
     */
    private static Map getProperties() throws IOException {
        PropertyValues pv = new PropertyValues();
        Map result = new HashMap();

        result.put("hibernate.connection.username", pv.getDBusername());
        result.put("hibernate.connection.password", pv.getDBpassword());

        return result;
    }
}
