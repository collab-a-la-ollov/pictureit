package Database.HibernateClasses;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The user class
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "hashpass", unique = true)
    private String hashpass;

    @Column(name = "salt", unique = true)
    private byte[] salt;

    @Column(name = "answer")
    private String forgotAnswer;

    @Column(name = "question")
    private String forgotQuestion;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashpass() {
        return hashpass;
    }

    public void setHashpass(String hashpass) {
        this.hashpass = hashpass;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public String getForgotAnswer() {
        return forgotAnswer;
    }

    public void setForgotAnswer(String forgotAnswer) {
        this.forgotAnswer = forgotAnswer;
    }

    public String getForgotQuestion() {
        return forgotQuestion;
    }

    public void setForgotQuestion(String forgotQuestion) {
        this.forgotQuestion = forgotQuestion;
    }
}