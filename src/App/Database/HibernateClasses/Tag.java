package Database.HibernateClasses;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The Tag class
 */
@Entity
@Table(name = "tags")
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "picture_id")
    private Long pictureId;

    @Column(name = "username")
    private String username;


    /**
     * Gets ID the tag has in the database
     *
     * @return the ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets ID of the assigned picture
     *
     * @param id the id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Gets the tag
     *
     * @return the tag as a String
     */
    public String getTag() {
        return tag;
    }

    /**
     * Sets the tag
     *
     * @param tag the tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * Gets ID of the picture the tag is assigned to
     *
     * @return the picture ID
     */
    public Long getPictureId() {
        return pictureId;
    }

    /**
     * Sets the pictureID.
     *
     * @param pictureId the picture id
     */
    public void setPictureId(Long pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * Gets the username of the user that uploaded the picture.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username of the user that uploaded the picture.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * toString method gives you the name as a tag.
     *
     * @return String name of the tag
     */
    @Override
    public String toString() {
        return getTag();
    }
}

