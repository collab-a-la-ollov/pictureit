package Database.HibernateClasses;


import javax.persistence.*;
import java.io.Serializable;

/**
 * The Picture class
 */
@Entity
@Table(name = "pictures")
public class Picture implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private long id;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "date")
    private String date;

    @Column(name = "url")
    private String url;

    @Column(name = "width")
    private int width;

    @Column(name = "height")
    private int height;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "file_size")
    private int fileSize;

    @Column(name = "username")
    private String username;


    /**
     * Gets the ID the picture has in the database.
     *
     * @return the ID
     */
    public long getId() {
        return id;
    }


    /**
     * Gets the date the picture was taken.
     *
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets date the picture was taken.
     *
     * @param date the date
     */
    public void setDate(String date) {
        this.date = date;
    }


    /**
     * Gets the latitude of the picture.
     *
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude of the picture.
     *
     * @param latitude the latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude of the picture.
     *
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude of the picture.
     *
     * @param longitude the longitude
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the url of the picture.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }


    /**
     * Sets url of the picture.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }


    /**
     * Gets the width of the picture.
     *
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the picture.
     *
     * @param width the width
     */
    public void setWidth(int width) {
        this.width = width;
    }


    /**
     * Gets the height of the picture.
     *
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the picture.
     *
     * @param height the height
     */
    public void setHeight(int height) {
        this.height = height;
    }


    /**
     * Gets the file type.
     *
     * @return the file type
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * Sets the file type.
     *
     * @param fileType the file type
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


    /**
     * Gets size of the file.
     *
     * @return the size of the file.
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * Sets the size of the file.
     *
     * @param fileSize the file size
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }


    /**
     * Gets the username of the user that uploaded the picture.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username of the user that uploaded the picture.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }


    /**
     * ToString method to get all the metadata as a String.
     *
     * @return String with metadata
     */
    @Override
    public String toString() {
        return "Height: " + getHeight() + "\nWidth: " +
                getWidth() + "\nDate: " + getDate() +
                "\nLatitude: " + getLatitude() +
                "\nLongitude: " + getLongitude() +
                "\nFile type: " + getFileType() +
                "\nFile size: " + getFileSize() + "\n";
    }
}
