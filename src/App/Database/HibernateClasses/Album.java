package Database.HibernateClasses;

import javax.persistence.*;
import java.io.Serializable;


/**
 * The album class
 */
@Entity
@Table(name = "albums")
public class Album implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "picture_id")
    private Long pictureId;

    @Column(name = "username")
    private String username;


    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param albumId the album id
     */
    public void setId(Long albumId) {
        this.id = albumId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gets picture id.
     *
     * @return the picture id
     */
    public Long getPictureId() {
        return pictureId;
    }

    /**
     * Sets picture id.
     *
     * @param pictureId the picture id
     */
    public void setPictureId(Long pictureId) {
        this.pictureId = pictureId;
    }


    /**
     * Gets the username of the user that uploaded the picture.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username of the user that uploaded the picture.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}