package Assets.Css;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;


/**
 * The Assets.Css class sets the layout for buttons, textfields ect...
 */
public class Css {
    private static String style = "-fx-cursor: hand;" +
            "-fx-border-style:solid inside;" +
            "-fx-border-width: 1px;" +
            "-fx-border-radius: 10px; " +
            "-fx-border-color: #C686A5;" +
            "-fx-background-color: #D5A6BD; " +
            "-fx-background-radius: 10px;" +
            "-fx-background-insets: 0";
    private static String hoverStyle = "-fx-cursor: hand;" +
            "-fx-border-style: solid inside;" +
            "-fx-border-width: 1px;" +
            "-fx-border-radius: 10px;" +
            "-fx-border-color: #C686A5;" +
            "-fx-background-color: #C686A5;" +
            "-fx-background-radius: 10px;" +
            "-fx-background-insets: 0";

    /**
     * Takes in an array of buttons and sets universal button.
     *
     * @param args array of buttons
     */
    public static void setUniversalButton(int fontSize, int width, int height, Button... args) {
        for (Button button : args) {
            button.setStyle(style);
            button.setFont(Font.font("Century Gothic", fontSize));
            button.setPrefHeight(height);
            button.setPrefWidth(width);
            button.setAlignment(Pos.CENTER);
            button.setOnMouseEntered((e) -> button.setStyle(hoverStyle));
            button.setOnMouseExited((e) -> button.setStyle(style));
        }
    }

    /**
     * Takes in an array of Menubuttons and sets universal button.
     *
     * @param args array of buttons
     */
    public static void setUniversalMenuButton(int fontSize, int width, int height, MenuButton... args) {
        for (MenuButton menuButton : args) {
            menuButton.setStyle(style);
            menuButton.setFont(Font.font("Century Gothic", fontSize));
            menuButton.setPrefHeight(height);
            menuButton.setPrefWidth(width);
            menuButton.setAlignment(Pos.CENTER);
            menuButton.setOnMouseEntered((e) -> {
                menuButton.setStyle(hoverStyle);
            });
            menuButton.setOnMouseExited((e) -> {
                menuButton.setStyle(style);
            });
        }
    }

    /**
     * Takes in an array of text fields and makes a text fields.
     *
     * @param args array of text fields
     */
    public static void setTextField(int fontSize, int width, int height, TextField... args) {
        String textFieldStyle = "-fx-background-color: #F1F1F1;" +
                "-fx-border-color: #656565;" +
                "-fx-border-radius: 10px;" +
                "-fx-background-radius: 10px";
        for (TextField textField : args) {
            textField.setStyle(textFieldStyle);
            textField.setPrefHeight(height);
            textField.setPrefWidth(width);
            textField.setFont(Font.font("Century Gothic", fontSize));

            textField.setOnMouseEntered((e) -> {
                textField.setStyle("-fx-border-width: 1px;" +
                        "-fx-border-radius: 10px;" +
                        "-fx-border-color: #000000;" +
                        "-fx-background-color: #E6E6E6;" +
                        "-fx-background-radius: 10px;" +
                        "-fx-background-insets: 0");
            });

            textField.setOnMouseExited((e) -> {
                textField.setStyle(textFieldStyle);
            });
        }
    }

    /**
     * Takes in an array of buttons and sets the main menu buttons.
     *
     * @param args array of buttons
     */
    public static void setMainMenuButtons(Button... args) {
        for (Button button : args) {
            String style = "-fx-cursor: hand;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 1px;" +
                    "-fx-border-radius: 10px;" +
                    " -fx-border-color: #DDDDDD;" +
                    "-fx-background-color: #D5A6BD;" +
                    " -fx-background-radius: 10px;" +
                    "-fx-background-insets: 0";
            button.setStyle(style);
            button.setFont(new Font("Century Gothic", 40.0D));
            button.setOnMouseEntered((e) -> {
                button.setStyle("-fx-cursor: hand;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 1px;" +
                        "-fx-border-radius: 15px;" +
                        "-fx-border-color: #DDDDDD;" +
                        "-fx-background-color: #A66E8A;" +
                        "-fx-background-radius: 15px;" +
                        "-fx-background-insets: 0;" +
                        " -fx-text-fill: white;");
            });
            button.setOnMouseExited((e) -> {
                button.setStyle(style);
            });
            button.setPrefHeight(280);
            button.setPrefWidth(190);
        }
    }

    /**
     * Takes in an array of buttons and makes a button for an album.
     *
     * @param arg array of buttons
     */
    public static void setAlbumButton(Button... arg) {
        for (Button button : arg) {
            String buttonStyle = "-fx-cursor: hand;" +
                    "-fx-border-style:solid inside;" +
                    "-fx-border-width: 1px;" +
                    "-fx-border-radius: 10px; " +
                    "-fx-border-color: #DEBACB ;" +
                    "-fx-background-color: #EFDCE5; " +
                    "-fx-background-radius: 10px;" +
                    "-fx-background-insets: 0;" +
                    "-fx-text-fill: black";
            button.setStyle(buttonStyle);
            button.setFont(Font.font("Century Gothic", 20));
            button.setPrefHeight(100);
            button.setPrefWidth(200);
            button.setAlignment(Pos.CENTER);
            button.setOnMouseEntered((e) -> {
                button.setStyle("-fx-cursor: hand;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 1px;" +
                        "-fx-border-radius: 10px;" +
                        "-fx-border-color: #793958;" +
                        "-fx-background-color: #EFDCE5;" +
                        "-fx-background-radius: 10px;" +
                        "-fx-background-insets: 0;" +
                        "-fx-text-fill: black");
            });
            button.setOnMouseExited((e) -> {
                button.setStyle(buttonStyle);
            });
        }
    }

    /**
     * Takes in an array of labels and sets a label.
     *
     * @param args array of labels.
     */
    public static void setLabel(int fontSize, String color, Label... args) {
        for (Label label : args) {
            label.setFont(Font.font("Century Gothic", fontSize));
            label.setStyle("-fx-text-fill:" + color);
            label.setAlignment(Pos.CENTER);
        }
    }

    /**
     * Sets info box.
     *
     * @param args array of VBoxes
     */
    public static void setInfoBox(VBox... args) {
        String style = "-fx-cursor: hand;" +
                "-fx-border-style:solid inside;" +
                "-fx-border-width: 2px;" +
                "-fx-border-radius: 5px; " +
                "-fx-border-color: #F4F4F4;" +
                "-fx-background-color: #F4F4F4; " +
                "-fx-background-radius: 5px;" +
                "-fx-background-insets: 0;" +
                "-fx-arc-height: 300";
        for (VBox vBox : args) {
            vBox.setStyle(style);
        }
    }

    /**
     * Sets text style.
     *
     * @param args array of Text
     */
    public static void setText(Text... args) {
        for (Text text : args) {
            text.setFont(Font.font("Century Gothic", 20));
        }
    }

    /**
     * Sets home button style.
     *
     * @param args list of buttons.
     */
    public static void setHomeButton(Button... args) {
        for (Button button : args) {
            button.setStyle("-fx-background-color: none;");
            button.setOnMouseEntered((e) -> {
                button.setStyle("-fx-background-color: #F2F2F2; -fx-cursor: hand");
            });
            button.setOnMouseExited((e) -> {
                button.setStyle("-fx-background-color: none");
            });
        }
    }

    /**
     * Sets menu button style.
     *
     * @param args array of menu buttons
     */
    public static void setMenuButton(MenuButton... args) {
        for (MenuButton menuButton : args) {
            menuButton.setStyle("-fx-background-color: none;");
            menuButton.setOnMouseEntered((e) -> {
                menuButton.setStyle("-fx-background-color: #F2F2F2; -fx-cursor: hand");
            });
            menuButton.setOnMouseExited((e) -> {
                menuButton.setStyle("-fx-background-color: none");
            });
        }
    }

    /**
     * Sets confirmation button.
     *
     * @param args array of buttons
     */
    public static void setConfirmationButton(Button... args) {
        for (Button button : args) {
            String style = "-fx-cursor: hand;" +
                    "-fx-border-style:solid inside;" +
                    "-fx-border-width: 1px;" +
                    "-fx-border-radius: 10px; " +
                    "-fx-border-color: #C686A5;" +
                    "-fx-background-color: #D5A6BD; " +
                    "-fx-background-radius: 10px;" +
                    "-fx-background-insets: 0";
            button.setStyle(style);
            button.setFont(Font.font("Century Gothic", 15));
            button.setPrefHeight(20);
            button.setPrefWidth(50);
            button.setAlignment(Pos.CENTER);
            button.setOnMouseEntered((e) -> {
                button.setStyle("" +
                        "-fx-cursor: hand;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 1px;" +
                        "-fx-border-radius: 10px;" +
                        "-fx-border-color: #C686A5;" +
                        "-fx-background-color: #C686A5;" +
                        "-fx-background-radius: 10px;" +
                        "-fx-background-insets: 0");
            });
            button.setOnMouseExited((e) -> {
                button.setStyle(style);
            });
        }
    }

    /**
     * Sets transparent button.
     *
     * @param args array of buttons
     */
    public static void setTransparentButton(Button... args) {
        for (Button button : args) {
            String style = "" +
                    "-fx-cursor: hand;" +
                    "-fx-border-style:solid inside;" +
                    "-fx-border-color: transparent;" +
                    "-fx-background-color: transparent;" +
                    "-fx-text-fill: #793958";
            button.setStyle(style);
            button.setFont(Font.font("Century Gothic", 15));
            button.setPrefHeight(50);
            button.setPrefWidth(200);
            button.setAlignment(Pos.CENTER);
            button.setTextAlignment(TextAlignment.RIGHT);
            button.setOnMouseEntered((e) -> {
                button.setStyle("" +
                        "-fx-cursor: hand;" +
                        "-fx-border-style: solid inside;" +
                        "-fx-border-width: 1px;" +
                        "-fx-border-color: transparent;" +
                        "-fx-background-color: transparent;" +
                        "-fx-font-weight: bold;" +
                        "-fx-text-fill: #793958 ");
            });
            button.setOnMouseExited((e) -> {
                button.setStyle(style);
            });
        }
    }
}