package DAO;

/**
 * Class DAO
 *
 * @author Team 11
 * @version 22.04.20
 */
public final class UserSession {
    private static UserSession userSession;
    private String userName;

    /**
     * Private constructor for DAO
     *
     * @param userName the users username
     */
    private UserSession(String userName) {
        this.userName = userName;
    }

    /**
     * Method used to make a new DAO
     *
     * @param userName username of the user logging in
     * @return DAO the DAO made
     */
    public static UserSession newUserSession(String userName) {
        userSession = new UserSession(userName);
        return userSession;
    }

    /**
     * Method to get the current DAO
     *
     * @return DAO the current DAO
     */
    public static UserSession getUserSession() {
        return userSession;
    }

    /**
     * Method to get the logged in user's username
     *
     * @return String the logged in user's username
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Method to remove the users username form the current DAO
     */
    public void cleanUserSession() {
        userName = null;
    }

    /**
     * Method to make DAO to String
     *
     * @return String the DAO as String
     */
    @Override
    public String toString() {
        return "DAO \n" +
                "userName: " + userName + "\n";
    }
}
