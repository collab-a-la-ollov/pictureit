package DAO;

import Database.HibernateClasses.User;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.List;

import static Database.Hibernate.getEm;
import static DAO.UserSession.getUserSession;

/**
 * Data access object for User
 *
 * @author Team 11
 * @version 22.04.20
 */
public class UserDAO {

    /**
     * Method to get logged in User
     *
     * @return the logged in User
     */
    public User getCurrentUser() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM users WHERE username = ?1", User.class)
                .setParameter(1, getUserSession().getUserName());
        List<User> user = q.getResultList();
        return user.get(0);
    }

    /**
     * Method to get a User by username
     *
     * @param username the users username
     * @return the User with entered username, null if username don't exist
     */
    public User getUserByUsername(String username) {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM users WHERE username = ?1", User.class)
                .setParameter(1, username);
        List<User> user = q.getResultList();
        if (user.size() > 0) {
            return user.get(0);
        } else return null;
    }

    /**
     * Method to check if entered username and password match
     *
     * @param username the entered username
     * @param password the entered password
     * @return true if the username and password match
     */
    public boolean checkInfo(String username, String password) {
        User user = getUserByUsername(username);
        try {
            return user.getHashpass().equals(hashPassword(password, user.getSalt()));
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * Method to check if username and answer match
     *
     * @param username the entered username
     * @param answer   the entered answer
     * @return true if the username and answer match
     */
    public boolean checkInfoHelp(String username, String answer) {
        try {
            return getUserByUsername(username).getForgotAnswer().equals(answer);
        } catch (NullPointerException e) {
            return false;
        }
    }

    /**
     * Method that checks if the username is in use
     *
     * @param username the username to be checked
     * @return true if it's not in use
     */
    public boolean checkUsername(String username) {
        return getUserByUsername(username) == null;
    }

    /**
     * Method that checks if the two passwords entered is the same
     *
     * @param password       the users password
     * @param repeatPassword repeated password
     * @return true if they are the same
     */
    public boolean checkPassword(String password, String repeatPassword) {
        return password.equals(repeatPassword) && !password.equals("");
    }

    /**
     * Gets the security question
     *
     * @param username of the user
     * @return the question
     */
    public String getQuestion(String username) {
        return getUserByUsername(username).getForgotQuestion();
    }

    /**
     * Sets the security question
     *
     * @param question new question
     */
    public void setQuestion(String question) {
        getCurrentUser().setForgotQuestion(question);
    }

    /**
     * Gets the answer to the security question
     *
     * @param username of the user
     * @return the answer
     */
    public String getAnswer(String username) {
        return getUserByUsername(username).getForgotAnswer();
    }

    /**
     * Sets the answer to the security question
     *
     * @param answer new answer
     */
    public void setAnswer(String answer) {
        getCurrentUser().setForgotAnswer(answer);
    }

    /**
     * Method that makes a new user and enters it into the database
     *
     * @param username the users username
     * @param password the users password
     */
    public void makeUser(String username, String password, String forgotQuestion, String forgotAnswer) {
        EntityManager em = getEm();
        em.getTransaction().begin();

        User newUser = new User();
        newUser.setUsername(username);
        newUser.setSalt(generateSalt());
        newUser.setHashpass(hashPassword(password, newUser.getSalt()));
        newUser.setForgotQuestion(forgotQuestion);
        newUser.setForgotAnswer(forgotAnswer);

        em.persist(newUser);
        em.getTransaction().commit();
    }

    /**
     * Method to delete logged in user
     */
    public void deleteUser() {
        EntityManager em = getEm();
        PictureDAO pictureDAO = new PictureDAO();
        pictureDAO.deletePictures(pictureDAO.getPictures());

        em.getTransaction().begin();
        User user = getCurrentUser();
        em.remove(user);
        em.getTransaction().commit();
    }

    /**
     * Method to change a users password
     *
     * @param newPassword the new password
     */
    public void changePassword(String newPassword) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        User user = getCurrentUser();
        user.setHashpass(hashPassword(newPassword, user.getSalt()));
        em.persist(user);
        em.getTransaction().commit();
    }

    /**
     * Method to hash a password with salt
     *
     * @param password password to be hashed
     * @param salt     salt to use when hashing
     * @return hashed password, null if unsuccessful
     */
    public String hashPassword(String password, byte[] salt) {
        if (password.equals("")) return null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte[] bytes = md.digest(salt);

            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Generates a salt, for hashing
     *
     * @return a random salt
     */
    public byte[] generateSalt() {
        return new SecureRandom().getSeed(16);
    }
}
