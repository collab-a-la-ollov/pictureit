package DAO;

import Database.HibernateClasses.Picture;
import Database.HibernateClasses.Tag;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static Database.Hibernate.getEm;
import static DAO.UserSession.getUserSession;

/**
 * Data access object for Tag
 *
 * @author Team 11
 * @version 22.04.20
 */
public class TagDAO {

    /**
     * Makes tag and enters it into the database
     *
     * @param name      the tag name
     * @param pictureID the id to the picture that is added to the tag
     * @return the tag
     */
    public Tag makeTag(String name, Long pictureID) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        Tag tag = new Tag();
        tag.setTag(name.trim());
        tag.setPictureId(pictureID);
        tag.setUsername(getUserSession().getUserName());
        em.persist(tag);
        em.getTransaction().commit();
        return tag;
    }

    /**
     * Method to delete tag from database
     *
     * @param tag tag to delete
     */
    public void deleteTag(Tag tag) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        em.remove(tag);
        em.getTransaction().commit();
    }

    /**
     * Method to delete tags from database
     *
     * @param tags list of tags to be deleted
     */
    public void deleteTags(List<Tag> tags) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        for (Tag t : tags) {
            em.remove(t);
        }
        em.getTransaction().commit();
    }

    /**
     * Gets all the logged in users tags
     *
     * @return list with the tags
     */
    public List<Tag> getTags() {
        EntityManager em = getEm();
        Query q2 = em.createNativeQuery("SELECT * FROM tags WHERE username = ?1", Tag.class)
                .setParameter(1, getUserSession().getUserName());
        return q2.getResultList();
    }

    /**
     * Gets all the tags to a picture
     *
     * @param picture the picture
     * @return a list of tags
     */
    public List<Tag> getTagsByPicture(Picture picture) {
        EntityManager em = getEm();
        Query q2 = em.createNativeQuery("SELECT * FROM tags WHERE username = ?1 AND picture_id = ?2", Tag.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, picture.getId());
        return q2.getResultList();
    }

    /**
     * Gets all tags with a given tag name
     *
     * @param name the tag name
     * @return list with the tags
     */
    public List<Tag> getTagsByTag(String name) {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM tags WHERE username = ?1 AND tag = ?2", Tag.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, name);
        return q.getResultList();
    }

    /**
     * Gets the different tag names
     *
     * @return list with tag names
     */
    public List<String> getTagNames() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT DISTINCT tag FROM tags WHERE username = ?1")
                .setParameter(1, getUserSession().getUserName());
        return q.getResultList();
    }
}
