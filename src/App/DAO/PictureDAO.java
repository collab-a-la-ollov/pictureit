package DAO;

import Components.ImageAnalyzer;
import Database.HibernateClasses.Picture;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.IOException;
import java.util.List;

import static Database.Hibernate.getEm;
import static DAO.UserSession.getUserSession;

/**
 * Data access object for Picture
 *
 * @author Team 11
 * @version 22.04.20
 */
public class PictureDAO {

    /**
     * Makes album and enters it into database
     *
     * @param url the pictures URL
     * @return the picture
     */
    public Picture makePicture(String url) {
        EntityManager em = getEm();
        try {
            em.getTransaction().begin();
            Picture picture = ImageAnalyzer.analyze(url);
            picture.setUsername(getUserSession().getUserName());
            em.persist(picture);
            em.getTransaction().commit();
            return picture;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Deletes picture from database
     *
     * @param picture picture to delete
     */
    public void deletePicture(Picture picture) {
        EntityManager em = getEm();
        AlbumDAO albumDAO = new AlbumDAO();
        albumDAO.deleteAlbums(albumDAO.getAlbumsByPicture(picture));
        TagDAO tagDAO = new TagDAO();
        tagDAO.deleteTags(tagDAO.getTagsByPicture(picture));

        em.getTransaction().begin();
        Picture delete = em.find(Picture.class, picture.getId());
        em.remove(delete);
        em.getTransaction().commit();
    }

    /**
     * Deletes pictures from database
     *
     * @param pictures List with pictures to be deleted
     */
    public void deletePictures(List<Picture> pictures) {
        for (Picture p : pictures) {
            deletePicture(p);
        }
    }

    /**
     * Gets picture by id
     *
     * @param pictureId the pictures id
     * @return the picture
     */
    public Picture getPictureById(Long pictureId) {
        EntityManager em = getEm();
        Picture picture = em.find(Picture.class, pictureId);
        em.detach(picture);
        return picture;
    }

    /**
     * Gets the logged in users pictures
     *
     * @return list with pictures
     */
    public List<Picture> getPictures() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM pictures WHERE username = ?1", Picture.class)
                .setParameter(1, getUserSession().getUserName());
        return q.getResultList();
    }

    /**
     * Gets pictures sorted by date
     *
     * @return pictures sorted by date
     */
    public List<Picture> picturesSortedByDate() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM pictures p WHERE p.username = ?1 ORDER BY date DESC", Picture.class)
                .setParameter(1, getUserSession().getUserName());
        return q.getResultList();
    }

    /**
     * Gets the pictures from one specific location
     *
     * @return list of pictures with a location
     */

    public List<Picture> getPicturesLocation() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM pictures p WHERE p.username = ?1 " +
                "AND p.latitude != ?2 AND p.longitude != ?3", Picture.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, 0)
                .setParameter(3, 0);
        return q.getResultList();
    }
}
