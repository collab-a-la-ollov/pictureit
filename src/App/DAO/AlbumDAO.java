package DAO;

import Database.HibernateClasses.Album;
import Database.HibernateClasses.Picture;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

import static Database.Hibernate.getEm;
import static DAO.UserSession.getUserSession;

/**
 * Data access object for Album
 *
 * @author Team 11
 * @version 22.04.20
 */
public class AlbumDAO {

    /**
     * Makes a album and enters it into the database
     *
     * @param name      the album name
     * @param pictureID the id to the picture that is added to the album
     */
    public void makeAlbum(String name, Long pictureID) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        Album album = new Album();
        album.setName(name);
        album.setPictureId(pictureID);
        album.setUsername(getUserSession().getUserName());
        em.persist(album);
        em.getTransaction().commit();
    }

    /**
     * Method to delete albums from database
     *
     * @param albums List with albums to be deleted
     */
    public void deleteAlbums(List<Album> albums) {
        EntityManager em = getEm();
        em.getTransaction().begin();
        for (Album a : albums) {
            em.remove(a);
        }
        em.getTransaction().commit();
    }

    /**
     * Gets the logged in users albums
     *
     * @return list with albums
     */
    public List<Album> getAlbums() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM albums WHERE username = ?1", Album.class)
                .setParameter(1, getUserSession().getUserName());
        return q.getResultList();
    }

    /**
     * Gets albums with given name
     *
     * @param name the name
     * @return list with the albums
     */
    public List<Album> getAlbumsByName(String name) {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM albums WHERE username = ?1 AND name = ?2", Album.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, name);
        return q.getResultList();
    }

    /**
     * Gets the albums connected to the picture
     *
     * @param picture the picture the albums should be connected to
     * @return list with the albums
     */
    public List<Album> getAlbumsByPicture(Picture picture) {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM albums WHERE username = ?1 AND picture_id = ?2", Album.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, picture.getId());
        return q.getResultList();
    }

    /**
     * Gets the the different album names
     *
     * @return list with album names
     */
    public List<String> getAlbumNames() {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT DISTINCT name FROM albums WHERE username = ?1")
                .setParameter(1, getUserSession().getUserName());
        return q.getResultList();
    }

    /**
     * Checks if the album exists
     *
     * @param name the album name
     * @return true if it exists
     */
    public boolean existingAlbum(String name) {
        EntityManager em = getEm();
        Query q = em.createNativeQuery("SELECT * FROM albums WHERE username = ?1 AND name = ?2", Album.class)
                .setParameter(1, getUserSession().getUserName())
                .setParameter(2, name);
        return !q.getResultList().isEmpty();
    }

    /**
     * Method to update an existing album
     *
     * @param tag       to check if there is a album for the tag
     * @param pictureID id on the picture that would be added to the album
     */
    public void updateAlbum(String tag, Long pictureID) {
        if (existingAlbum(tag)) {
            makeAlbum(tag, pictureID);
        }
    }
}
