package Main;

import Scenes.StageInitializer;
import javafx.application.Application;
import javafx.stage.Stage;


/**
 * The class who ruins the applications
 */
public class Main extends Application {

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * start method. Overrides the start method from its super class
     * Starts a stage from stage initializer
     *
     * @param stage the stage
     * @throws Exception .
     */
    @Override
    public void start(Stage stage) throws Exception {
        StageInitializer.initialize(stage);
    }
}
