package Scenes;

import Assets.Css.Css;
import DAO.UserDAO;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

import static DAO.UserSession.getUserSession;


/**
 * Class upload scene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class ProfileScene extends SceneBuilder {
    private UserDAO userDAO = new UserDAO();

    private Label passwordTextLabel = new Label("Change password: ");
    private PasswordField passwordField = new PasswordField();
    private PasswordField repeatPasswordField = new PasswordField();
    private Button passwordButton = new Button("Change");
    private Label securityLabel = new Label("Your Security question with answer:");
    private Label questionLabel = new Label(userDAO.getQuestion(getUserSession().getUserName()));
    private Label answerLabel = new Label("Answer: " + userDAO.getAnswer(getUserSession().getUserName()));
    private Label forgotQuestionLabel = new Label("Type in a security question:");
    private TextField forgotQuestionField = new TextField();
    private TextField forgotAnswerField = new TextField();
    private Button securityButton = new Button("Change");
    private Button changeButton = new Button("Change");
    private Button deleteButton = new Button("Delete user");
    private Label feedbackLabel = new Label();
    private Button backButton = new Button("Back");

    private GridPane gridPane = super.getSecondGridPane();

    /**
     * Constructor for Upload scene.
     */
    public ProfileScene() {
        super();
        this.setLayout(true);
    }

    /**
     * Sets the layout for the Upload Scene. Overrides the setLayout()-method from SceneBuilder,
     * but also uses it to modify the method.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("@" + getUserSession().getUserName());

        passwordField.setPromptText("New password");
        repeatPasswordField.setPromptText("Repeat password");
        forgotQuestionField.setPromptText("Question");
        forgotAnswerField.setPromptText("Answer");

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(backButton, deleteButton);
        buttonBox.setSpacing(150);

        super.setScrollPane(gridPane);
        ScrollPane scrollPane = super.getScrollPane();
        scrollPane.fitToWidthProperty().set(false);

        gridPane.addColumn(0, passwordTextLabel, passwordField, repeatPasswordField, passwordButton,
                securityLabel, questionLabel, answerLabel, securityButton);
        gridPane.setAlignment(Pos.TOP_CENTER);

        Css.setUniversalButton(20, 180, 30, passwordButton, securityButton, changeButton, backButton, deleteButton);
        Css.setLabel(28, "#656565", passwordTextLabel, questionLabel, answerLabel, forgotQuestionLabel, securityLabel);
        Css.setTextField(20, 200, 25, passwordField, repeatPasswordField, forgotQuestionField, forgotAnswerField);
        Css.setLabel(20, "black", questionLabel, answerLabel);

        passwordButton.setOnAction(event -> changePassword());

        securityButton.setOnAction(event -> changeQuestionLayout());

        changeButton.setOnAction(event -> changeQuestion());

        backButton.setOnAction(event -> StageInitializer.setMainMenuScene());

        deleteButton.setOnAction(event -> deleteUser());

        super.getGridPane().addColumn(0, scrollPane, new Text(" "), buttonBox, feedbackLabel);
    }

    /**
     * Method to change password
     */
    public void changePassword() {
        String newPassword = passwordField.getText();
        if (userDAO.checkPassword(newPassword, repeatPasswordField.getText())) {
            userDAO.changePassword(newPassword);
            Css.setLabel(15, "#B6638B", feedbackLabel);
            feedbackLabel.setText("Password changed");
        } else {
            Css.setLabel(16, "red", feedbackLabel);
            feedbackLabel.setText("Passwords dont match");
        }
        passwordField.clear();
        repeatPasswordField.clear();
    }

    /**
     * Method to delete user
     */
    private void deleteUser() {
        boolean delete = ConfirmationBox.display("Delete user", "Are you sure you want to delete this user?");
        if (delete) {
            userDAO.deleteUser();
            StageInitializer.setLogInScene();
            getUserSession().cleanUserSession();
        }
    }

    /**
     * Method that sets layout for changing security question
     */
    private void changeQuestionLayout() {
        gridPane.getChildren().removeAll(passwordTextLabel, passwordField, repeatPasswordField, passwordButton,
                securityLabel, questionLabel, answerLabel, securityButton);
        gridPane.addColumn(0, forgotQuestionLabel, forgotQuestionField,
                forgotAnswerField, changeButton);

        backButton.setOnAction(event -> StageInitializer.setProfileScene());
    }

    /**
     * Method to change security question
     */
    private void changeQuestion() {
        if (forgotQuestionField.getText().trim().equals("") || forgotAnswerField.getText().trim().equals("")) {
            feedbackLabel.setText("Fill in fields");
            Css.setLabel(20, "red", feedbackLabel);
        } else {
            userDAO.setQuestion(forgotQuestionField.getText().trim());
            userDAO.setAnswer(forgotAnswerField.getText().trim());
            StageInitializer.setProfileScene();
        }
    }
}
