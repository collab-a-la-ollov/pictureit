package Scenes;

import Assets.Css.Css;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.ArrayList;

/**
 * Main menu class
 *
 * @author Team 11
 * @version 22.04.20
 */
public class MainMenu extends SceneBuilder {
    private ArrayList<ImageView> pics = new ArrayList<>();
    private ArrayList<VBox> boxes = new ArrayList<>();
    private ArrayList<Label> labels = new ArrayList<>();
    private ArrayList<Button> buttons = new ArrayList<>();

    /**
     * Instantiates a new Main menu.
     */
    public MainMenu() {
        super();
        this.setLayout(true);
    }

    /**
     * Sets the layout for the main menu.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);

        pics.add(new ImageView(new Image("file:src/App/Assets/Images/iconUpload.png")));
        pics.add(new ImageView(new Image("file:src/App/Assets/Images/iconPictures2.png")));
        pics.add(new ImageView(new Image("file:src/App/Assets/Images/iconMakeAlbums.png")));
        pics.add(new ImageView(new Image("file:src/App/Assets/Images/iconAlbums.png")));
        pics.add(new ImageView(new Image("file:src/App/Assets/Images/iconMap.png")));

        boxes.add(new VBox());
        boxes.add(new VBox());
        boxes.add(new VBox());
        boxes.add(new VBox());
        boxes.add(new VBox());

        labels.add(new Label("Upload"));
        labels.add(new Label("Pictures"));
        labels.add(new Label(" Make\nAlbum"));
        labels.add(new Label("Albums"));
        labels.add(new Label("Map"));

        //Sets the button layout
        for (int i = 0; i < pics.size(); i++) {
            pics.get(i).setFitWidth(100);
            pics.get(i).setPreserveRatio(true);
            pics.get(i).setSmooth(true);
            pics.get(i).setCache(true);

            labels.get(i).setPadding(new Insets(40, 0, 0, 0));
            boxes.get(i).setAlignment(Pos.CENTER);
            boxes.get(i).getChildren().addAll(pics.get(i), labels.get(i));
            buttons.add(new Button("", boxes.get(i)));
            Css.setMainMenuButtons(buttons.get(i));
            Css.setLabel(28, "#656565", labels.get(i));

            super.getGridPane().add(buttons.get(i), i, 0);
        }

        //Adjusting because make album is to long
        labels.get(2).setPadding(new Insets(12, 0, 0, 0));
        labels.get(2).setFont(Font.font("Century Gothic", 27));

        //Setting all the buttons on actions
        buttons.get(0).setOnAction(e -> StageInitializer.setUploadScene());
        buttons.get(1).setOnAction(e -> StageInitializer.setPictureScene());
        buttons.get(2).setOnAction(e -> StageInitializer.setMakeAlbumScene());
        buttons.get(3).setOnAction(e -> StageInitializer.setAlbumScene());
        buttons.get(4).setOnAction(e -> StageInitializer.setMapScene());
    }
}