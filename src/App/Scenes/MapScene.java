package Scenes;

import Assets.Css.Css;
import Database.HibernateClasses.Picture;
import DAO.PictureDAO;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import netscape.javascript.JSObject;
import Propertiy.PropertyValues;

import java.io.IOException;
import java.util.List;

/**
 * Class MapScene
 */
public class MapScene extends SceneBuilder implements MapComponentInitializedListener {
    private GoogleMapView mapView;
    private GoogleMap map;
    private Button backButton = new Button("Back");

    /**
     * Constructor for MapScene
     */
    public MapScene() {
        super();
        this.setLayout(true);
    }

    /**
     * Method that sets the layout for MapScene
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        mapView = new GoogleMapView();
        mapView.addMapInializedListener(this);

        try {
            mapView.setKey(new PropertyValues().getMapKey());
            super.getGridPane().add(mapView, 0, 0);
        } catch (IOException e) {
            super.getGridPane().add(new Label("No map"), 0, 0);
        }

        super.setLayout(activateHomeButton);
        super.setPageTitle("Map");
        super.getGridPane().add(emptyInfoBox(true), 1, 0);
        super.getGridPane().getChildren().removeAll(backButton);
        super.getGridPane().add(backButton, 0, 1);
        super.getGridPane().setPadding(new Insets(30, 30, 30, 30));

        Css.setUniversalButton(20, 180, 30, backButton);

        backButton.setOnAction(event -> StageInitializer.setMainMenuScene());
    }

    /**
     * Method that creates the map
     */
    @Override
    public void mapInitialized() {
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(new LatLong(49.228464, 9.734709))
                .overviewMapControl(true)
                .panControl(true)
                .rotateControl(true)
                .scaleControl(true)
                .streetViewControl(false)
                .zoomControl(true)
                .zoom(4);

        map = mapView.createMap(mapOptions);
        addPictureMarkers(new PictureDAO().getPicturesLocation());
    }

    /**
     * Method that adds markers for the pictures
     *
     * @param pictures List with pictures
     */
    private void addPictureMarkers(List<Picture> pictures) {
        if (pictures.size() == 0) {
            getGridPane().add(emptyInfoBox(false), 1, 0);
        }
        for (Picture p : pictures) {
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(new LatLong(p.getLatitude(), p.getLongitude()))
                    .visible(Boolean.TRUE);
            Marker marker = new Marker(markerOptions);
            map.addMarker(marker);

            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            String imageUrl = "<p> <img src=" + p.getUrl() + " height = 100 width = 100> </p>";
            infoWindowOptions.content(imageUrl);
            InfoWindow infoWindow = new InfoWindow(infoWindowOptions);
            infoWindow.open(map, marker);
            map.addUIEventHandler(marker, UIEventType.click, (JSObject obj) -> {
                infoWindow.open(map, marker);
                getGridPane().add(infoBox(p), 1, 0);
            });
        }
    }

    /**
     * Method to show info box when no picture is selected
     *
     * @param pictures false if none of the users pictures have location info
     * @return VBox with information
     */
    private VBox emptyInfoBox(boolean pictures) {
        VBox pictureInfo = new VBox();
        Label title = new Label("Info");
        Text info;
        if (pictures) {
            info = new Text("Click on a marker to see picture info...");
        } else {
            info = new Text("There are no pictures with location");
        }
        Css.setInfoBox(pictureInfo);
        Css.setLabel(28, "#B6638B", title);
        Css.setText(info);
        pictureInfo.getChildren().addAll(title, info);
        return pictureInfo;
    }

    /**
     * Method to show info box when a picture is selected
     *
     * @param picture the picture selected
     * @return VBox with information
     */
    private VBox infoBox(Picture picture) {
        VBox pictureInfo = new VBox();
        Label title = new Label("Info");
        Button viewPictureButton = new Button("View picture");

        Text info = new Text(picture.toString());

        viewPictureButton.setOnAction(actionEvent -> {
            new ViewPictureBox(picture.getId());
            this.setLayout(true);
        });

        Css.setInfoBox(pictureInfo);
        Css.setLabel(28, "#B6638B", title);
        Css.setText(info);
        Css.setUniversalButton(20, 180, 30, viewPictureButton);

        pictureInfo.getChildren().addAll(title, info, viewPictureButton);
        return pictureInfo;
    }
}


