package Scenes;

import Assets.Css.Css;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * The type Confirmation box.
 *
 * @author Team 11
 * @version 22.04.20
 */
public class ConfirmationBox {
    private static boolean answer;

    /**
     * Throws IllegalStateException if constructor is used
     */
    private ConfirmationBox() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Shows the display of the confirmation box.
     *
     * @param title   the window title
     * @param message the message
     * @return true if the answer is yes
     */
    public static boolean display(String title, String message) {
        // Pop up window
        Stage window = new Stage();
        window.getIcons().add(StageInitializer.getIcon());
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(300);
        window.setMinHeight(200);

        Label messageLabel = new Label(message);
        Css.setLabel(20, "#656565", messageLabel);

        Button yesButton = new Button("Yes");
        Button noButton = new Button("No");
        Css.setConfirmationButton(yesButton);
        Css.setConfirmationButton(noButton);

        yesButton.setOnAction(s -> {
            answer = true;
            window.close();
        });

        noButton.setOnAction(s -> {
            answer = false;
            window.close();
        });

        HBox buttons = new HBox();
        buttons.getChildren().addAll(yesButton, noButton);
        buttons.setAlignment(Pos.CENTER);
        buttons.setSpacing(10);

        VBox vBox = new VBox(10);
        vBox.getChildren().addAll(messageLabel, buttons);
        vBox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.showAndWait();

        return answer;
    }
}
