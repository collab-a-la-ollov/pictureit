package Scenes;

import Assets.Css.Css;
import Database.HibernateClasses.Picture;
import Database.HibernateClasses.Tag;
import DAO.PictureDAO;
import DAO.TagDAO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Class PictureScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class PictureScene extends SceneBuilder {
    private PictureDAO pictureDAO = new PictureDAO();
    private TagDAO tagDAO = new TagDAO();
    private List<Picture> pictureList;
    private Button backButton = new Button("Back");
    private Button sortByDate = new Button("Sort by date");
    private MenuButton filterByTag = new MenuButton("Filter by tag");
    private Button removeFilter = new Button("Remove filter");
    private HBox buttonBox = new HBox(20);
    private GridPane gridPane;
    private ScrollPane scrollPane;

    /**
     * Constructor for picture scene
     */
    public PictureScene() {
        super();
        pictureList = pictureDAO.getPictures();
        this.setLayout(true);
    }

    /**
     * Sets the layout of the page, overriding the setLayout()-method from super class,
     * using the modifications declared in this method.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Pictures");

        filterByTag.getStylesheets().add("file:src/App/Assets/Css/MenuButton.css");
        Css.setUniversalButton(20, 180, 30, sortByDate, removeFilter, backButton);
        buttonBox.getChildren().addAll(backButton, sortByDate, filterByTag, removeFilter);
        buttonBox.setAlignment(Pos.CENTER);
        removeFilter.setDisable(true);

        //Sets menu items for filter by tag
        List<String> tags = tagDAO.getTagNames();
        for (String tag : tags) {
            MenuItem menuItem = new MenuItem(tag);
            filterByTag.getItems().add(menuItem);
            menuItem.setOnAction(e -> {
                pictureList.clear();
                pictureList = this.filterByTag(tag);
                showPictures(activateHomeButton);
                super.setPageTitle("#" + tag);
                removeFilter.setDisable(false);
            });
        }

        backButton.setOnAction(event -> StageInitializer.setMainMenuScene());

        sortByDate.setOnAction(event -> {
            pictureList = pictureDAO.picturesSortedByDate();
            showPictures(activateHomeButton);
            super.setPageTitle("Pictures sorted by date");
            removeFilter.setDisable(false);
        });

        removeFilter.setOnAction(e -> {
            pictureList = pictureDAO.getPictures();
            showPictures(activateHomeButton);
            super.setPageTitle("Pictures");
            removeFilter.setDisable(true);
        });

        gridPane = super.getSecondGridPane();

        super.setScrollPane(gridPane);
        scrollPane = super.getScrollPane();

        super.getGridPane().getChildren().removeAll(buttonBox, scrollPane);
        super.getGridPane().add(buttonBox, 0, 0);
        super.getGridPane().add(scrollPane, 0, 1);

        showPictures(activateHomeButton);
    }

    /**
     * Method for showing all the pictures
     *
     * @param activateHomeButton tells if the header should have an active home button
     */
    private void showPictures(boolean activateHomeButton) {
        //Adds pictures to grid pane
        if (pictureList.size() == 0) {
            sortByDate.setMouseTransparent(true);
            removeFilter.setMouseTransparent(true);
            scrollPane.setContent(noPicturesMessage());
        } else {
            sortByDate.setMouseTransparent(false);
            removeFilter.setMouseTransparent(false);
            gridPane.getChildren().clear();
            ArrayList<ImageView> imageViews = new ArrayList<>();
            for (int i = 0; i < pictureList.size(); i++) {
                int row = i / 5;
                int column = i - (5 * row);

                try {
                    imageViews.add(new ImageView(new Image(pictureList.get(i).getUrl(), 200, 200, true, true, true)));
                    gridPane.add(imageViews.get(i), column, row);
                } catch (NullPointerException | IllegalArgumentException ignored) {
                }

                int index = i;
                imageViews.get(i).setOnMouseClicked(actionEvent -> {
                    new ViewPictureBox(pictureList.get(index).getId());
                    pictureList = pictureDAO.getPictures();
                    showPictures(activateHomeButton);
                });
            }
        }

    }

    /**
     * Method for filtering out all the pictures with a specific tag.
     *
     * @param tag The specific tag
     * @return ArrayList with all the pictures with this tag.
     */
    private ArrayList<Picture> filterByTag(String tag) {
        List<Tag> tags = tagDAO.getTagsByTag(tag);

        ArrayList<Long> pictureIds = new ArrayList<>();
        tags.forEach(t -> pictureIds.add(t.getPictureId()));

        ArrayList<Picture> pictures = new ArrayList<>();
        for (Long pictureId : pictureIds) {
            pictures.add(pictureDAO.getPictureById(pictureId));
        }
        return pictures;
    }

    /**
     * Method to show message box when no pictures are uploaded
     *
     * @return vBox with message
     */
    private VBox noPicturesMessage() {
        VBox noPicturesBox = new VBox();
        Label noPicturesLabel = new Label("You have no pictures :(");
        Button uploadButton = new Button("Upload pictures");
        noPicturesBox.setPadding(new Insets(10, 10, 10, 10));
        noPicturesBox.setSpacing(10);
        Css.setLabel(20, "black", noPicturesLabel);
        Css.setUniversalButton(17, 200, 50, uploadButton);
        noPicturesBox.getChildren().addAll(noPicturesLabel, uploadButton);
        noPicturesBox.setAlignment(Pos.CENTER);
        sortByDate.setDisable(true);
        filterByTag.setDisable(true);

        uploadButton.setOnAction(event -> StageInitializer.setUploadScene());
        return noPicturesBox;
    }
}
