package Scenes;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

import java.util.ConcurrentModificationException;

/**
 * Class Scene Builder.
 * Superclass for scene classes
 *
 * @author Team 11
 * @version 22.04.20
 */
public abstract class SceneBuilder {
    private final int WIDTH = 1200;
    private final int HEIGHT = 700;
    private Header header;
    private BorderPane borderPane;
    private GridPane gridPane;
    private GridPane secondGridPane;
    private ScrollPane scrollPane;
    private Scene scene;

    /**
     * SceneBuilder constructor
     */
    public SceneBuilder() {
        this.gridPane = new GridPane();
        this.setGridPane();
        this.secondGridPane = new GridPane();
        this.setSecondGridPane();
        this.scrollPane = new ScrollPane();
        this.borderPane = new BorderPane();
        this.scene = new Scene(borderPane, WIDTH, HEIGHT);
    }

    /**
     * Sets layout.
     */
    public void setLayout(boolean activateHomeButton) {
        header = new Header(activateHomeButton);
        BorderPane.setAlignment(header.getHeaderBox(), Pos.CENTER);
        BorderPane.setMargin(header.getHeaderBox(), new Insets(10.0D, 10.0D, 10.0D, 10.0D));
        borderPane.setTop(header.getHeaderBox());
        borderPane.setCenter(gridPane);
    }

    /**
     * Sets page title.
     *
     * @param title the title
     */
    public void setPageTitle(String title) {
        header.setPageTitle(title);
    }

    /**
     * Gets grid pane.
     *
     * @return the grid pane
     */
    public GridPane getGridPane() {
        return gridPane;
    }

    /**
     * Sets padding and alignment for the grid pane
     */
    public void setGridPane() {
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setAlignment(Pos.CENTER);
    }

    /**
     * Gets the second grid pane
     *
     * @return the second grid pane
     */
    public GridPane getSecondGridPane() {
        return secondGridPane;
    }

    /**
     * Sets second grid pane
     */
    public void setSecondGridPane() {
        secondGridPane.setPadding(new Insets(30, 30, 30, 30));
        secondGridPane.setHgap(10);
        secondGridPane.setVgap(10);
    }

    /**
     * Gets the scroll pane
     *
     * @return the scroll pane
     */
    public ScrollPane getScrollPane() {
        return scrollPane;
    }

    /**
     * Sets scroll pane
     *
     * @param content the content of the scroll pane
     */
    public void setScrollPane(Node content) {
        scrollPane.setContent(content);
        scrollPane.setStyle("-fx-background-color:transparent");
        scrollPane.setHbarPolicy(javafx.scene.control.ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.fitToWidthProperty().set(true);
    }

    /**
     * Gets scene.
     *
     * @return the scene
     */
    public Scene getScene() throws ConcurrentModificationException {
        return scene;
    }
}
