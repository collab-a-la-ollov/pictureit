package Scenes;

import Assets.Css.Css;
import DAO.UserDAO;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import static DAO.UserSession.newUserSession;

/**
 * Class LogInScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class ForgotPasswordScene extends SceneBuilder {
    private UserDAO userDAO = new UserDAO();

    private Label usernameLabel = new Label("Username: ");
    private TextField usernameField = new TextField();
    private Button nextButton = new Button("Next");
    private Label questionLabel = new Label();
    private TextField answerField = new TextField();
    private Button loginButton = new Button("Log in");
    private Button backButton = new Button("Back");
    private Label feedbackLabel = new Label();

    /**
     * Constructor for LogInScene
     */
    public ForgotPasswordScene() {
        super();
        this.setLayout(false);
    }

    /**
     * Method that sets the layout for the sign in scene
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Forgot password");

        super.getGridPane().addColumn(0, usernameLabel, usernameField, nextButton, new Text(" "), backButton, feedbackLabel);

        usernameField.setPromptText("Username");
        answerField.setPromptText("Answer");

        Css.setLabel(28,"#656565", usernameLabel, questionLabel, feedbackLabel);
        Css.setTextField(20, 700, 25, usernameField, answerField);
        Css.setUniversalButton(20, 180, 30, loginButton, backButton, nextButton);

        nextButton.setOnAction(event -> setQuestionLayout());

        loginButton.setOnAction(event -> login());

        backButton.setOnAction(event -> StageInitializer.setLogInScene());

    }

    /**
     * Method to log in user
     */
    private void login() {
        String username = usernameField.getText().trim();
        String answer = answerField.getText().trim();
        if (!username.equals("") && !answer.equals("") && userDAO.checkInfoHelp(username, answer)) {
            newUserSession(username);
            StageInitializer.setProfileScene();
        } else {
            feedbackLabel.setText("Wrong username or answer");
        }
    }

    /**
     * Sets the layout for entering answer to security question
     */
    private void setQuestionLayout() {
        feedbackLabel.setVisible(false);
        String username = usernameField.getText().trim();
        if (!username.equals("") && !userDAO.checkUsername(username)) {
            VBox questionBox = new VBox();
            questionBox.getChildren().addAll(questionLabel, answerField, loginButton);
            questionBox.setPadding(new Insets(0, 10, 10, 0));
            questionBox.setSpacing(10);

            super.getGridPane().add(questionBox, 0, 3);
            super.getGridPane().getChildren().remove(nextButton);

            questionLabel.setText(userDAO.getUserByUsername(username).getForgotQuestion());

            backButton.setOnAction(event -> {
                StageInitializer.setForgotPasswordScene();
                feedbackLabel.setVisible(false);
            });
        } else {
            feedbackLabel.setVisible(true);
            feedbackLabel.setText("Wrong username");
        }
    }
}
