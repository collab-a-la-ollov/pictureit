package Scenes;

import Assets.Css.Css;
import DAO.UserDAO;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

import static DAO.UserSession.newUserSession;

/**
 * Class LogInScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class LogInScene extends SceneBuilder {

    private Label usernameLabel = new Label("Username: ");
    private TextField usernameField = new TextField();
    private Label passwordLabel = new Label("Password: ");
    private PasswordField passwordField = new PasswordField();
    private Button loginButton = new Button("Log in");
    private Button signUpButton = new Button("Sign up");
    private Button forgotButton = new Button("Forgot password?");
    private Label feedbackLabel = new Label();

    /**
     * Constructor for LogInScene
     */
    public LogInScene() {
        super();
        this.setLayout(false);
    }

    /**
     * Method that sets the layout for the sign in scene
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Log In");

        usernameField.setPromptText("Username");
        passwordField.setPromptText("Password");

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(loginButton, signUpButton, forgotButton);
        buttonBox.setSpacing(20);

        super.getGridPane().addColumn(0, usernameLabel, usernameField, passwordLabel,
                passwordField, buttonBox, new Text(""), feedbackLabel);

        Css.setLabel(28, "#656565", usernameLabel, passwordLabel, feedbackLabel);
        Css.setTextField(20, 700, 25, usernameField, passwordField);
        Css.setUniversalButton(20, 180, 30, loginButton, signUpButton, forgotButton);
        Css.setTransparentButton(forgotButton);

        loginButton.setOnAction(event -> login());
        loginButton.setDefaultButton(true); //Makes it possible to use enter to log in

        signUpButton.setOnAction(event -> StageInitializer.setSignUpScene());

        forgotButton.setOnAction(event -> StageInitializer.setForgotPasswordScene());
    }

    /**
     * Method to log in user
     */
    private void login() {
        UserDAO userDAO = new UserDAO();
        String username = usernameField.getText().trim();
        String password = passwordField.getText().trim();
        if (!username.equals("") && !password.equals("") && userDAO.checkInfo(username, password)) {
            newUserSession(username);
            StageInitializer.setMainMenuScene();
        } else feedbackLabel.setText("Wrong username or password");
    }
}
