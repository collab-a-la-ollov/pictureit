package Scenes;

import Assets.Css.Css;
import Database.HibernateClasses.Picture;
import Database.HibernateClasses.Tag;
import DAO.AlbumDAO;
import DAO.PictureDAO;
import DAO.TagDAO;
import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import Propertiy.PropertyValues;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;


/**
 * Class upload scene
 */
public class UploadScene extends SceneBuilder {
    private PictureDAO pictureDAO = new PictureDAO();
    private AlbumDAO albumDAO = new AlbumDAO();
    private TagDAO tagDAO = new TagDAO();
    private File file;
    private Label urlTextLabel = new Label("URL: ");
    private TextField urlTextField = new TextField();
    private Button browseButton = new Button("Browse");
    private Button uploadButton = new Button("Upload image");
    private Label tagTextLabel = new Label("Tags: ");
    private TextField tagTextField = new TextField();
    private Label feedbackLabel = new Label();
    private Button backButton = new Button("Back");

    /**
     * Constructor to Upload scene.
     */
    public UploadScene() {
        super();
        this.setLayout(true);
    }

    /**
     * Sets the layout for the Upload Scene. Overrides the setLayout()-method from SceneBuilder,
     * but also uses it to modify the method.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Upload picture");

        urlTextField.setPromptText("URL...");
        tagTextField.setPromptText("Tag1, tag2...");

        super.getGridPane().addColumn(0, urlTextLabel, urlTextField, tagTextLabel, tagTextField,
                new Text(" "), uploadButton, backButton, new Text(" "), feedbackLabel);
        super.getGridPane().add(browseButton, 1, 1);
        super.getGridPane().setAlignment(Pos.TOP_CENTER);

        Css.setUniversalButton(20, 180, 30, browseButton, uploadButton, backButton);
        Css.setTextField(20, 700, 25, urlTextField, tagTextField);
        Css.setLabel(28, "#656565", tagTextLabel, urlTextLabel);

        browseButton.setOnAction(event -> browse());

        uploadButton.setOnAction(event -> {
            if (checkField()) savePicture();
        });

        super.getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && checkField()) savePicture();
        });

        backButton.setOnAction(event -> StageInitializer.setMainMenuScene());
    }


    /**
     * Checks the text fields. Sets feedback labels accordingly.
     *
     * @return true if the test is successful
     */
    private boolean checkField() {
        feedbackLabel.setVisible(true);
        if (urlTextField.getText().length() == 0) {
            feedbackLabel.setText("URL missing");
            Css.setLabel(20, "red", feedbackLabel);
            return false;
        }
        try {
            Image imageTest = new Image(urlTextField.getText());
            if (imageTest.isError()) {
                Css.setLabel(20, "red", feedbackLabel);
                feedbackLabel.setText("URL is invalid");
                return false;
            }
        } catch (Exception ex) {
            Css.setLabel(20, "red", feedbackLabel);
            feedbackLabel.setText("URL is invalid");
            return false;
        }
        Css.setLabel(20, "#B6638B", feedbackLabel);
        feedbackLabel.setText("Image uploaded successfully");
        return true;
    }

    /**
     * Method that checks the URL
     *
     * @return String, the URL or null if it doesnt pas the test
     */
    private String urlIn() {
        if (checkField()) {
            return urlTextField.getText().trim();
        } else throw new NullPointerException();
    }

    /**
     * Saves a picture to the database.
     */
    private void savePicture() {
        try {
            String url;
            PropertyValues pv = new PropertyValues();
            if (!urlIn().contains("https")) {
                Cloudinary cloudinary = new Cloudinary(pv.getProperties());
                Map result = cloudinary.uploader().upload(file, ObjectUtils.emptyMap());
                url = result.get("url").toString();
            } else {
                url = urlIn();
            }
            Picture picture = pictureDAO.makePicture(url);
            if (picture != null) {
                saveTagToPicture(picture.getId());
            } else {
                feedbackLabel.setText("Could not upload picture");
            }
            tagTextField.clear();
            urlTextField.clear();
        } catch (IOException e) {
            feedbackLabel.setText("Could not read file");
        } catch (NullPointerException e) {
            feedbackLabel.setText("Something went wrong");
        }

    }

    /**
     * Save tag to picture
     * .
     *
     * @param pictureID the picture id
     */
    private void saveTagToPicture(long pictureID) {
        if (!tagTextField.getText().equals("")) {
            String[] tags = tagTextField.getText().split(",");
            for (String tagName : tags) {
                Tag tag = tagDAO.makeTag(tagName.trim(), pictureID);
                albumDAO.updateAlbum(tag.getTag(), pictureID);
            }
        }
    }

    /**
     * Method that opens window to brows computer for picture
     */
    private void browse() {
        Stage popUp = new Stage();
        popUp.initModality(Modality.APPLICATION_MODAL);
        FileChooser fileChooser = new FileChooser();
        file = fileChooser.showOpenDialog(popUp);
        if (file != null) {
            try {
                urlTextField.setText(file.toURI().toURL().toString());
            } catch (MalformedURLException e) {
                urlTextField.clear();
                feedbackLabel.setText("Invalid URL");
            }
        }
    }
}
