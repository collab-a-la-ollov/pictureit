package Scenes;

import Assets.Css.Css;
import DAO.AlbumDAO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.List;

/**
 * Class AlbumScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class AlbumScene extends SceneBuilder {
    private final List<String> albumList;
    private Button backButton = new Button("Back");

    /**
     * Constructor: using the constructor declared in the super class.
     */
    public AlbumScene() {
        super();
        this.albumList = new AlbumDAO().getAlbumNames();
        this.setLayout(true);
    }

    /**
     * Sets the layout of the page, overriding the setLayout()-method from super class.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Your albums");

        GridPane gridPane = super.getSecondGridPane();
        super.setScrollPane(gridPane);
        ScrollPane scrollPane = super.getScrollPane();

        // Adds album buttons to gridPane
        if (albumList.size() == 0) {
            scrollPane.setContent(noAlbumsMessage());
        } else {
            for (int i = 0; i < albumList.size(); i++) {
                int index = i;
                int row = i / 5;
                int column = i - (5 * row);

                try {
                    Button b = new Button(albumList.get(index));
                    Css.setAlbumButton(b);
                    gridPane.add(b, column, row);
                    gridPane.setAlignment(Pos.CENTER);

                    b.setOnAction(actionEvent -> StageInitializer.setViewAlbumScene(albumList.get(index)));

                } catch (NullPointerException ignore) {
                }
            }
        }

        Css.setUniversalButton(20, 180, 30, backButton);
        backButton.setOnAction(e -> StageInitializer.setMainMenuScene());

        super.getGridPane().addColumn(0, scrollPane, backButton);
    }

    /**
     * Method that views a message if there is no albums.
     *
     * @return VBox with message
     */
    private VBox noAlbumsMessage() {
        VBox noAlbumsBox = new VBox();
        Label noAlbumsLabel = new Label("You have no albums :(");
        Button makeAlbumButton = new Button("Make album");

        noAlbumsBox.setPadding(new Insets(10, 10, 10, 0));
        noAlbumsBox.setSpacing(10);
        Css.setLabel(20, "black", noAlbumsLabel);
        Css.setUniversalButton(20, 200, 30, makeAlbumButton);
        noAlbumsBox.getChildren().addAll(noAlbumsLabel, makeAlbumButton);
        noAlbumsBox.setAlignment(Pos.CENTER);

        makeAlbumButton.setOnAction(e -> StageInitializer.setMakeAlbumScene());

        return noAlbumsBox;
    }
}

