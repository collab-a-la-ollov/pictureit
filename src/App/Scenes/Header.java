package Scenes;

import Assets.Css.Css;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.util.ArrayList;

import static DAO.UserSession.getUserSession;

/**
 * Class Header
 *
 * @author Team 11
 * @version 22.04.20
 */
public class Header {
    private MenuButton menuButton = new MenuButton();
    private Button homeButton;
    private HBox buttonBox = new HBox();
    private Label pageTitle = new Label();
    private VBox headerBox = new VBox();

    /**
     * Instantiates a new Header.
     *
     * @param activateHomeButton true if home button should be active
     */
    public Header(boolean activateHomeButton) {
        //Settings for the logo-pic
        ImageView homeIcon = new ImageView(StageInitializer.getIcon());
        homeIcon.setFitWidth(100);
        homeIcon.setFitHeight(80);
        homeIcon.setPreserveRatio(true); //Preserving ratio
        homeIcon.setSmooth(true); //Higher quality filtering method
        homeIcon.setCache(true); //improve performance

        //Label
        Label pictureIt = new Label("PictureIt");
        pictureIt.setFont(Font.loadFont("file:src/App/Assets/Fonts/sitka-small-599.ttf", 70));
        pictureIt.setStyle("-fx-text-fill: #474747");

        //Adding icon and label to hBox then to button
        HBox logoBox = new HBox();
        logoBox.getChildren().clear();
        logoBox.getChildren().addAll(homeIcon, pictureIt);
        homeButton = new Button("", logoBox);

        buttonBox.getChildren().clear();
        //Activates home button and shows menu button
        if (activateHomeButton) {
            homeButton.setMouseTransparent(false);
            homeButton.setOnAction(e -> StageInitializer.setMainMenuScene());

            buttonBox.getChildren().addAll(menuButton, homeButton);
            menuButton.getItems().clear();
            setMenuButton();
            menuButton.setPadding(new Insets(80, 0, 0, 0));
            menuButton.setAlignment(Pos.CENTER);
            Css.setMenuButton(menuButton);
        } else { //Deactivates home button
            homeButton.setMouseTransparent(true);
            buttonBox.getChildren().add(homeButton);
        }

        this.setButtonBox();
        this.setHeaderBox();
    }

    /**
     * Sets page title.
     *
     * @param newTitle the new title
     */
    public void setPageTitle(String newTitle) {
        this.pageTitle.setText(newTitle);
    }

    /**
     * Gets header box.
     *
     * @return the header box
     */
    public VBox getHeaderBox() {
        return headerBox;
    }

    /**
     * Sets the header box, with spacing, alignment and children.
     */
    private void setHeaderBox() {
        Css.setLabel(28, "#B6638B", pageTitle);
        headerBox.setSpacing(10.0D);
        headerBox.setAlignment(Pos.CENTER);
        headerBox.getChildren().clear();
        headerBox.getChildren().addAll(buttonBox, pageTitle);
        headerBox.setMaxWidth(1000);
        headerBox.setPrefHeight(100.0D);
    }

    /**
     * Sets the button box, with spacing, alignment and children.
     */
    private void setButtonBox() {
        Css.setHomeButton(homeButton);
        buttonBox.setSpacing(10.0D);
        buttonBox.setAlignment(Pos.CENTER);
        homeButton.setPadding(new Insets(100, 0, 0, 0));
        homeButton.setAlignment(Pos.CENTER);
    }

    /**
     * Creates the drop down menu.
     */
    private void setMenuButton() {
        ImageView openView = new ImageView(new Image("file:src/App/Assets/Images/menuIcon.png"));

        openView.setFitWidth(50);
        openView.setFitHeight(100);
        openView.setPreserveRatio(true); //Preserving ratio
        openView.setSmooth(true); //Higher quality filtering method
        openView.setCache(true);
        menuButton.setGraphic(openView);

        ArrayList<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MenuItem("@" + getUserSession().getUserName()));
        menuItems.add(new MenuItem("Home"));
        menuItems.add(new MenuItem("Upload"));
        menuItems.add(new MenuItem("Pictures"));
        menuItems.add(new MenuItem("Make Album"));
        menuItems.add(new MenuItem("Albums"));
        menuItems.add(new MenuItem("Map"));
        menuItems.add(new MenuItem("Sign out"));

        menuButton.getItems().addAll(menuItems.get(0), new SeparatorMenuItem(), menuItems.get(1), menuItems.get(2), menuItems.get(3),
                menuItems.get(4), menuItems.get(5), menuItems.get(6), new SeparatorMenuItem(), menuItems.get(7));
        menuButton.getStylesheets().add("file:src/App/Assets/Css/ChoiceBox.css");

        menuItems.get(0).setOnAction(e -> Scenes.StageInitializer.setProfileScene());
        menuItems.get(1).setOnAction(e -> Scenes.StageInitializer.setMainMenuScene());
        menuItems.get(2).setOnAction(e -> Scenes.StageInitializer.setUploadScene());
        menuItems.get(3).setOnAction(e -> Scenes.StageInitializer.setPictureScene());
        menuItems.get(4).setOnAction(e -> Scenes.StageInitializer.setMakeAlbumScene());
        menuItems.get(5).setOnAction(e -> Scenes.StageInitializer.setAlbumScene());
        menuItems.get(6).setOnAction(e -> StageInitializer.setMapScene());
        menuItems.get(7).setOnAction(e -> {
            StageInitializer.setLogInScene();
            getUserSession().cleanUserSession();
        });

        menuItems.forEach(m -> m.setStyle("-fx-font-size: 20.0 pt;"));
    }
}
