package Scenes;

import Assets.Css.Css;
import DAO.UserDAO;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import static DAO.UserSession.newUserSession;

/**
 * Class SignUpScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class SignUpScene extends SceneBuilder {
    private UserDAO userDAO = new UserDAO();

    private Label usernameLabel = new Label("Username* ");
    private TextField usernameField = new TextField();
    private Label passwordLabel = new Label("Password* ");
    private PasswordField passwordField = new PasswordField();
    private PasswordField repeatPasswordField = new PasswordField();
    private Label forgotLabel = new Label("Type in a security question:");
    private TextField forgotQuestionField = new TextField();
    private Label answereLabel = new Label("Your answer");
    private TextField forgotAnswerField = new TextField();
    private Button loginButton = new Button("Log in");
    private Button signUpButton = new Button("Sign up");
    private Label feedbackLabel = new Label();

    /**
     * Constructor for SignUpScene
     * It calls for the methods that sets the layout
     */
    public SignUpScene() {
        super();
        this.setLayout(false);
    }

    /**
     * Method that sets the layout for the signupscene
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Sign up");

        usernameField.setPromptText("Username");
        passwordField.setPromptText("Password");
        repeatPasswordField.setPromptText("Repeat password");
        forgotQuestionField.setPromptText("Write you question");
        forgotAnswerField.setPromptText("Write your answer");

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(signUpButton, loginButton);
        buttonBox.setSpacing(10);

        VBox signUpBox = new VBox();
        signUpBox.getChildren().addAll(usernameLabel, usernameField,
                passwordLabel, passwordField, repeatPasswordField, forgotLabel,
                forgotQuestionField, answereLabel, forgotAnswerField);
        signUpBox.setSpacing(10);
        signUpBox.setPadding(new Insets(10, 10, 30, 10));

        Css.setLabel(28, "#656565", usernameLabel, passwordLabel, forgotLabel, answereLabel, feedbackLabel);
        Css.setTextField(20, 700, 25, usernameField, passwordField, repeatPasswordField, forgotQuestionField, forgotAnswerField);
        Css.setUniversalButton(20, 180, 30, loginButton, signUpButton);

        super.setScrollPane(signUpBox);
        ScrollPane scrollPane = super.getScrollPane();

        loginButton.setOnAction(event -> StageInitializer.setLogInScene());

        signUpButton.setOnAction(event -> signUp());

        super.getGridPane().addColumn(0, scrollPane, buttonBox, feedbackLabel);
    }

    /**
     * Method to register a new user
     */
    private void signUp() {
        String username = usernameField.getText();
        String password = passwordField.getText();
        if (!username.trim().equals("") && userDAO.checkUsername(username) && userDAO.checkPassword(password, repeatPasswordField.getText())) {
            userDAO.makeUser(username, password, forgotQuestionField.getText(), forgotAnswerField.getText());
            newUserSession(username);
            StageInitializer.setMainMenuScene();
        } else if (!userDAO.checkUsername(username) || username.trim().equals("")) {
            feedbackLabel.setText("Username already in use");
        } else {
            feedbackLabel.setText("Passwords don't match");
        }
    }
}
