package Scenes;

import Assets.Css.Css;
import Database.HibernateClasses.Picture;
import Database.HibernateClasses.Tag;
import DAO.AlbumDAO;
import DAO.PictureDAO;
import DAO.TagDAO;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Class ViewPictureBox
 *
 * @author Team 11
 * @version 22.04.20
 */
class ViewPictureBox {
    private PictureDAO pictureDAO = new PictureDAO();
    private AlbumDAO albumDAO = new AlbumDAO();
    private TagDAO tagDAO = new TagDAO();

    private Stage popUp;
    private long pictureId;
    private Picture p;
    private ScrollPane scrollPane = new ScrollPane();
    private TextField newTagName = new TextField();
    private Label feedBackLabel = new Label();

    /**
     * Making the stage showing the picture and the metadata.
     *
     * @param pictureId id of the picture to show
     */
    ViewPictureBox(Long pictureId) {
        popUp = new Stage();
        popUp.initModality(Modality.APPLICATION_MODAL);
        this.pictureId = pictureId;
        this.p = pictureDAO.getPictureById(pictureId);

        Button closeButton = new Button("Close");
        closeButton.setOnAction(event -> popUp.close());

        Button deleteButton = new Button("Delete picture");
        deleteButton.setOnAction(event -> deleteButtonClicked());

        Label title = new Label("Info");
        Css.setLabel(25, "#656565", title);
        ImageView picture = new ImageView(new Image(p.getUrl(), 300, 300, true, true, true));
        Text info = new Text();
        info.setText(p.toString());
        Css.setText(info);

        VBox infoBox = new VBox();
        infoBox.getChildren().addAll(title, info);

        scrollPane.setContent(tagsBox());
        scrollPane.fitToWidthProperty().set(true);
        scrollPane.vbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setStyle("-fx-background-color: transparent;");

        HBox hBox = new HBox();
        hBox.getChildren().addAll(picture, infoBox);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPadding(new Insets(10, 10, 10, 10));
        hBox.setSpacing(15);

        //HBox for the buttons
        HBox buttonBox = new HBox();
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setPadding(new Insets(5, 5, 5, 5));
        buttonBox.setSpacing(5);
        Button addTagButton = new Button("Add");
        buttonBox.getChildren().addAll(closeButton, newTagName, addTagButton, deleteButton);

        Css.setUniversalButton(20, 180, 30, closeButton, deleteButton);
        Css.setTextField(20, 200, 25, newTagName);
        Css.setUniversalButton(20, 100, 30, addTagButton);
        Css.setLabel(15, "red", feedBackLabel);

        newTagName.setPromptText("Add tag");
        addTagButton.setOnAction(event -> addTag());

        VBox layout = new VBox();
        layout.getChildren().addAll(hBox, scrollPane, buttonBox, feedBackLabel);
        layout.setAlignment(Pos.CENTER);
        layout.setMinSize(800, 500);
        layout.setPadding(new Insets(10, 10, 10, 10));

        Scene scene = new Scene(layout);
        popUp.setScene(scene);
        popUp.showAndWait();
    }

    /**
     * Method to delete picture
     */
    private void deleteButtonClicked() {
        boolean delete = ConfirmationBox.display("Delete picture", "Are you sure you want to\ndelete this picture?");
        if (delete) {
            pictureDAO.deletePicture(p);
            popUp.close();
        }
    }

    /**
     * Makes a HBox with a tag and a delete button
     *
     * @param tag the tag
     * @return a HBox
     */
    private HBox tagButton(Tag tag) {
        HBox tagContainer = new HBox();
        Button deleteButton = new Button("X");
        Css.setHomeButton(deleteButton);
        deleteButton.setOnAction(e -> {
            tagDAO.deleteTag(tag);
            scrollPane.setContent(tagsBox());
        });

        Text tagName = new Text(tag.toString());
        tagName.setFont(Font.font("Century Gothic", 17));
        tagContainer.getChildren().addAll(tagName, deleteButton);
        tagContainer.setStyle("-fx-padding: 5px; -fx-background-color: #D9D9D9; -fx-background-radius: 10;");
        return tagContainer;
    }

    /**
     * Creates a HBox with tagButtons
     *
     * @return HBox
     */
    private HBox tagsBox() {
        HBox tags = new HBox();
        tagDAO.getTagsByPicture(pictureDAO.getPictureById(pictureId)).forEach(t -> tags.getChildren().add(tagButton(t)));
        tags.setSpacing(10);
        tags.setAlignment(Pos.CENTER);
        tags.setPadding(new Insets(0, 0, 20, 0));
        return tags;
    }

    /**
     * Adds a tag to picture
     */
    private void addTag() {
        String newTag = newTagName.getText().trim();
        if (!newTag.equals("") && tagDAO.getTagsByPicture(p).stream().noneMatch(t -> t.getTag().equals(newTag))) {
            tagDAO.makeTag(newTag, pictureId);
            albumDAO.updateAlbum(newTag, pictureId);
            scrollPane.setContent(tagsBox());
            newTagName.clear();
        } else if (newTag.equals("")) {
            feedBackLabel.setText("Fill text field");
        } else {
            feedBackLabel.setText("Tag already assigned");
            newTagName.clear();
        }
    }
}


