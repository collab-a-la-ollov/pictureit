package Scenes;

import Database.Hibernate;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Class StageInitializer.
 * Initializes all the stages in the application.
 *
 * @author Team 11
 * @version 22.04.20
 */
public class StageInitializer {
    private static Stage stage;
    private static Image icon = new Image("file:src/App/Assets/Images/Logo3.png");

    /**
     * Initializes the stages.
     *
     * @param primaryStage the primary stage
     */
    public static void initialize(Stage primaryStage) {
        stage = primaryStage;
        stage.setOnCloseRequest(s -> {
            s.consume();
            closeProgram();
        });
        stage.getIcons().add(icon);
        stage.setTitle("PictureIt");
        setLogInScene();
        stage.show();
    }

    /**
     * Gets stage.
     *
     * @return the stage
     */
    public static Stage getStage() {
        return stage;
    }

    /**
     * Gets the icon
     *
     * @return the icon
     */
    public static Image getIcon() {
        return icon;
    }

    /**
     * Sets sign in scene.
     */
    static void setLogInScene() {
        stage.setScene(new LogInScene().getScene());
    }

    /**
     * Sets sign up scene.
     */
    static void setSignUpScene() {
        stage.setScene(new SignUpScene().getScene());
    }

    /**
     * Sets forgot password scene
     */
    static void setForgotPasswordScene() {
        stage.setScene(new ForgotPasswordScene().getScene());
    }

    /**
     * Sets profile scene
     */
    static void setProfileScene() {
        stage.setScene(new ProfileScene().getScene());
    }

    /**
     * Sets main menu scene.
     */
    static void setMainMenuScene() {
        stage.setScene(new MainMenu().getScene());
    }

    /**
     * Sets upload scene.
     */
    static void setUploadScene() {
        stage.setScene(new UploadScene().getScene());
    }

    /**
     * Sets picture scene.
     */
    static void setPictureScene() {
        stage.setScene(new PictureScene().getScene());
    }

    /**
     * Sets album scene.
     */
    static void setAlbumScene() {
        stage.setScene(new AlbumScene().getScene());
    }

    /**
     * Sets make album scene.
     */
    static void setMakeAlbumScene() {
        stage.setScene(new MakeAlbumScene().getScene());
    }

    /**
     * Set view album scene.
     *
     * @param name the album name
     */
    static void setViewAlbumScene(String name) {
        stage.setScene(new ViewAlbumScene(name).getScene());
    }

    /**
     * Sets map scene
     */
    static void setMapScene() {
        stage.setScene(new MapScene().getScene());
    }

    /**
     * Closes program.
     */
    private static void closeProgram() {
        boolean close = ConfirmationBox.display("Exit", "Are you sure you want to exit?");
        if (close) {
            stage.close();
            Hibernate.getEm().clear();
            Hibernate.getEntityManagerFactory().close();
        }
    }

}