package Scenes;

import Components.PDF;
import Assets.Css.Css;
import DAO.AlbumDAO;
import DAO.PictureDAO;
import Database.HibernateClasses.Album;
import com.itextpdf.text.DocumentException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class ViewAlbumScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class ViewAlbumScene extends SceneBuilder {
    private PictureDAO pictureDAO = new PictureDAO();
    private AlbumDAO albumDAO = new AlbumDAO();

    private static Button makePDFButton = new Button("Make PDF");
    private Button backButton = new Button("Back");
    private Button deleteButton = new Button("Delete");
    private ArrayList<Long> pictureIds;

    /**
     * Constructor for view album scene
     *
     * @param name name of the album
     */
    public ViewAlbumScene(String name) {
        super();
        this.pictureIds = getPictureIds(albumDAO.getAlbumsByName(name));
        this.setLayout(name);
    }

    /**
     * Sets the layout of the page, overriding the setLayout()-method from super class,
     * using the modifications declared in this method.
     *
     * @param name name of the album
     */
    public void setLayout(String name) {
        super.setLayout(true);
        super.setPageTitle(name);

        GridPane gridPane = super.getSecondGridPane();

        super.setScrollPane(gridPane);
        ScrollPane scrollPane = super.getScrollPane();

        //Adds pictures to grid pane
        ArrayList<String> pictures = new ArrayList<>();
        ArrayList<ImageView> imageViews = new ArrayList<>();
        for (int i = 0; i < pictureIds.size(); i++) {
            int row = i / 5;
            int column = i - (5 * row);

            pictures.add(pictureDAO.getPictureById(pictureIds.get(i)).getUrl());
            imageViews.add(new ImageView(new Image(pictureDAO.getPictureById(pictureIds.get(i)).getUrl(),
                    200, 200, true, true, true)));
            gridPane.add(imageViews.get(i), column, row);

            int index = i;
            imageViews.get(i).setOnMouseClicked(actionEvent -> {
                new ViewPictureBox(pictureIds.get(index));
                this.pictureIds = getPictureIds(albumDAO.getAlbumsByName(name));
                if (pictureIds.size() == 0) StageInitializer.setAlbumScene();
                else this.setLayout(name);
            });
        }

        makePDFButton.setOnAction(event -> {
            try {
                new PDF().createPDF(pictures, name);
            } catch (IOException | DocumentException ignore) {
            }
        });

        backButton.setOnAction(event -> StageInitializer.setAlbumScene());
        deleteButton.setOnAction(event -> deleteButtonClicked(name));

        Css.setUniversalButton(20, 180, 30, makePDFButton, backButton, deleteButton);

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(10);
        hBox.getChildren().addAll(backButton, makePDFButton, deleteButton);

        //super.getGridPane().addColumn(0, scrollPane, hBox);
        super.getGridPane().getChildren().remove(scrollPane);
        super.getGridPane().add(scrollPane, 0, 0);
        super.getGridPane().add(hBox, 0, 1);
    }

    /**
     * Method that returns the ID of the pictures within an album.
     *
     * @param albums List of all albums
     * @return ArrayList with pictureIds
     */
    private static ArrayList<Long> getPictureIds(List<Album> albums) {
        ArrayList<Long> pictureIds = new ArrayList<>();
        albums.stream().forEach(a -> pictureIds.add(a.getPictureId()));
        return pictureIds;
    }

    /**
     * Method to delete album
     *
     * @param name the album name
     */
    private void deleteButtonClicked(String name) {
        boolean delete = ConfirmationBox.display("Delete picture", "Are you sure you want to\ndelete this album?");
        if (delete) {
            albumDAO.deleteAlbums(albumDAO.getAlbumsByName(name));
            StageInitializer.setAlbumScene();
        }
    }

}