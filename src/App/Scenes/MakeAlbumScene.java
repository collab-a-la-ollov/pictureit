package Scenes;

import Assets.Css.Css;
import DAO.AlbumDAO;
import DAO.TagDAO;
import Database.HibernateClasses.Album;
import Database.HibernateClasses.Tag;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;

/**
 * Class MakeAlbumScene
 *
 * @author Team 11
 * @version 22.04.20
 */
public class MakeAlbumScene extends SceneBuilder {

    private AlbumDAO albumDAO = new AlbumDAO();
    private TagDAO tagDAO = new TagDAO();

    private Label feedbackLabel = new Label();

    private List<Album> albums;
    private List<String> tags;

    /**
     * Constructor for the make album scene.
     */
    public MakeAlbumScene() {
        super();
        this.albums = albumDAO.getAlbums();
        this.tags = tagDAO.getTagNames();
        this.setLayout(true);
    }

    /**
     * Overrides the setLayout()-method from SceneBuilder.
     *
     * @param activateHomeButton true if home button should be active
     */
    @Override
    public void setLayout(boolean activateHomeButton) {
        super.setLayout(activateHomeButton);
        super.setPageTitle("Make Album");

        Label searchBoxLabel = new Label("Choose tag to make album:");
        Button makeAlbumButton = new Button("Make Album");
        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        Button backButton = new Button("Back");
        String preSelect;

        HBox hBox = new HBox();
        hBox.getChildren().addAll(choiceBox, feedbackLabel);
        hBox.setSpacing(10);

        if (getAvailableTags().size() == 0) {
            preSelect = "No tags available";
            choiceBox.getItems().add(preSelect);
            choiceBox.setMouseTransparent(true);
            makeAlbumButton.setMouseTransparent(true);

        } else {
            preSelect = "Select tag";
            choiceBox.getItems().add(preSelect);
            choiceBox.getItems().addAll(getAvailableTags());
            makeAlbumButton.setMouseTransparent(false);
        }

        choiceBox.setValue(preSelect);
        choiceBox.getStylesheets().add("file:src/App/Assets/Css/ChoiceBox.css");

        Css.setUniversalButton(20, 180, 30, makeAlbumButton, backButton);
        Css.setLabel(28, "#656565", searchBoxLabel);

        makeAlbumButton.setOnAction(event -> makeAlbum(choiceBox.getValue()));

        backButton.setOnAction(event -> StageInitializer.setMainMenuScene());

        super.getGridPane().getChildren().removeAll(searchBoxLabel, hBox, makeAlbumButton, backButton);
        super.getGridPane().add(searchBoxLabel, 0, 0);
        super.getGridPane().add(hBox, 0, 1);
        super.getGridPane().add(makeAlbumButton, 0, 3);
        super.getGridPane().add(backButton, 0, 4);
        super.getGridPane().setAlignment(Pos.TOP_CENTER);
    }

    /**
     * Makes a new album based on the chosen tag. Fills the album
     * with pictures with that specific tag.
     */
    private void makeAlbum(String tag) {
        if (!tag.equals("Select tag...")) {
            feedbackLabel.setVisible(true);
            List<Tag> tags = tagDAO.getTagsByTag(tag);

            for (Tag t : tags) {
                albumDAO.makeAlbum(tag, t.getPictureId());
            }

            feedbackLabel.setText("Album " + tag + " made");
            Css.setLabel(20, "#B6638B", feedbackLabel);

            albums = albumDAO.getAlbums();
            this.setLayout(true);

        } else {
            feedbackLabel.setText("Select tag");
            Css.setLabel(20, "red", feedbackLabel);
        }
    }

    /**
     * Method which return all tags that have not been
     * used to make an album.
     *
     * @return List of tags
     */
    private ArrayList<String> getAvailableTags() {
        ArrayList<String> availableTags = new ArrayList<>();
        for (String t : tags) {
            if (albums.stream().noneMatch(a -> a.getName().equals(t))) {
                availableTags.add(t);
            }
        }
        return availableTags;
    }
}