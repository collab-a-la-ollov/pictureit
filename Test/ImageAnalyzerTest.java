import Components.ImageAnalyzer;
import Database.HibernateClasses.Picture;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ImageAnalyzerTest {
    private Picture picture = new Picture();

    @Before
    public void  getTestMetadata(){
        try {
            picture = ImageAnalyzer.analyze("https://i.ibb.co/sFnqVQ6/20200308-143923.jpg");
        }
        catch (IOException e){

        }
    }

    @Test
    public void testLongitude (){
        assertNotNull( picture.getLongitude());
    }

    @Test
    public void testLatidute (){
        assertNotNull( picture.getLatitude());
    }

    @Test
    public void testDate (){
        assertEquals("2020:03:08 14:39:22", picture.getDate());
    }


    @Test
    public void testFileSize (){
        assertEquals(3929826, picture.getFileSize());
    }

    @Test
    public void testFileType (){
        assertEquals("image/jpeg", picture.getFileType());
    }

    @Test
    public void testHeight (){
        assertEquals(3468, picture.getHeight());
    }

    @Test
    public void testWidth (){
        assertEquals(4624, picture.getWidth());
    }

    @Test
    public void testUrl (){
        assertEquals("https://i.ibb.co/sFnqVQ6/20200308-143923.jpg", picture.getUrl());
    }

}
