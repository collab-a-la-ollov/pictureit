import DAO.UserDAO;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserDAOTest {
    private UserDAO userDAO;

    @Before
    public void makeUserDAO() {
        userDAO = new UserDAO();
    }

    @Test
    public void checkPasswordTest() {
        assertTrue(userDAO.checkPassword("hello", "hello"));
        assertFalse(userDAO.checkPassword("hello", "1234"));
    }

    @Test
    public void hashPasswordTest() {
        String password1 = "hello";
        String password2 = "1234";

        byte[] saltTest1 = userDAO.generateSalt();
        byte[] saltTest2 = userDAO.generateSalt();

        String hashedPassoword1 = userDAO.hashPassword(password1, saltTest1);
        String hashedPassoword2 = userDAO.hashPassword(password1, saltTest2);
        String hashedPassoword3 = userDAO.hashPassword(password2, saltTest1);
        String hashedPassoword4 = userDAO.hashPassword(password1, saltTest1);

        assertEquals(hashedPassoword1, hashedPassoword4);
        assertNotEquals(hashedPassoword1, hashedPassoword2);
        assertNotEquals(hashedPassoword1, hashedPassoword3);
    }

    @Test
    public void testGenerateSalt () {
        byte[] saltTest1 = userDAO.generateSalt();
        byte[] saltTest2 = userDAO.generateSalt();

        assertNotEquals(saltTest1, saltTest2);
    }
}
