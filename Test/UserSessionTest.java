import DAO.UserSession;
import org.junit.Before;
import org.junit.Test;

import static DAO.UserSession.getUserSession;
import static DAO.UserSession.newUserSession;
import static org.junit.Assert.*;

public class UserSessionTest {
    private static UserSession userSession;
    private String username = "TestUser";

    @Before
    public void makeUserSession() {
        userSession = newUserSession(username);
    }

    @Test
    public void newUserSessionTest() {
        assertNotNull(newUserSession(username));
    }

    @Test
    public void getUserSessionTest () {
        assertEquals(userSession, getUserSession());
    }

    @Test
    public void getUserNameTest () {
        assertEquals(username, userSession.getUserName());
    }

    @Test
    public void cleanUserSessionTest () {
        userSession.cleanUserSession();
        assertNull(userSession.getUserName());
    }
}
